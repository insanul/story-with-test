from django import forms

class Add_Status(forms.Form):

	attrs_status = {
        'id': 'status',
        'type': 'textarea',
        'class': 'form-control',
        'placeholder': 'Nama Pengisi - Status Anda',
    }

	status = forms.CharField(max_length=300, widget = forms.Textarea(attrs=attrs_status))