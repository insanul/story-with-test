from django.utils import timezone
from datetime import datetime, date, timedelta
from django.db import models

class Status(models.Model):

    status = models.CharField(max_length=300)
    waktu = models.DateTimeField(default=timezone.now()+timedelta(hours=7))

    def __str__(self):
        return self.status