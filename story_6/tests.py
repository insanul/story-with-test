from django.test import TestCase, Client
from django.urls import resolve, reverse
import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time
from .views import index
from .forms import Add_Status
from .models import Status
from django.utils import timezone


# Create your tests here.

class Story6UnitTest(TestCase):

	def test_index_url_is_exist(self):
		response = Client().get('')
		self.assertEqual(response.status_code,200)

	def test_uses_status_page_view(self): #cek url memanggil fungsi yang mana
		handler = resolve('/') #nama argumen pertama di urls project
		self.assertEqual(handler.func, index) #nama fungsi di views

	def test_uses_status_page_template(self): #cek views merender html
		response = self.client.get('/') #nama argumen pertama di urls project
		self.assertTemplateUsed(response, 'index.html')
    
	def test_status_page_form(self):
		form_data = {
			'status': 'halo apa kabar semuanya?',
		}
		status_form = Add_Status(data=form_data)
		self.assertTrue(status_form.is_valid())

	def test_forms_post_status(self):
		response = self.client.post('',{
			'status': "SUKSES!",
			})
		self.assertIn("SUKSES!",
			response.content.decode())

	def test_post_status(self):
		status = 'halo semuanya!'
		waktu = timezone.now()

		postStatus = Status.objects.create(status=status, waktu=waktu)
		postStatus.save()

	def test_template_is_written(self):
		response = self.client.get("/")
		html_response = response.content.decode('utf8')
		self.assertIn('Halo, Apa Kabar?', html_response)

	def test_models_object_status(self):
		status_name = Status(status='halo, tes dulu yak')
		self.assertEqual(str(status_name), 'halo, tes dulu yak')


class FunctionalTest(TestCase):
	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('disable-gpu')

		self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
		super(FunctionalTest, self).setUp()

	def tearDown(self):
		self.selenium.quit()
		super(FunctionalTest, self).tearDown()

	def test_status_works(self):
		selenium = self.selenium
		selenium.get('http://127.0.0.1:8000')
		time.sleep(3)

		status_box = selenium.find_element_by_id('status')
		submit = selenium.find_element_by_id('button-form')

		status_box.send_keys('Coba Coba')
		time.sleep(3)

		submit.send_keys(Keys.RETURN)
		time.sleep(5)

		selenium.get('http://127.0.0.1:8000')
		assert 'Coba Coba' in selenium.page_source


