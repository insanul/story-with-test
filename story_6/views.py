from django.shortcuts import render
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from datetime import datetime, date
from .forms import *
from .models import *
from django.urls import reverse

# Create your views here.
def index(request):

	if request.method == 'POST':
		form = Add_Status(request.POST)
		if form.is_valid():
			status_person = form.cleaned_data["status"]
			stat = Status(status = status_person)
			stat.save()

	form = Add_Status()
	schd = Status.objects.all()
	return render(request, 'index.html', {'form' : form, 'table': schd})