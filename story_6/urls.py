from django.contrib import admin
from django.urls import *
from .views import *

app_name='story_6'
urlpatterns = [
	path('admin/', admin.site.urls),
    path('', index, name='index'),
]